import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <>
      <Head>
        <title>Maniraj Murugan</title>
        <meta
          name="description"
          content="Maniraj Murugan - Kongu - Maniyan Kootam - Karur - Senior Software Engineer"
        ></meta>
        <meta
          name="keywords"
          content="Maniraj, Maniraj Murugan, Web Developer in Karur, Kongu Matrimony, Kongu Vellalar Matrimony, Kongu Grooms in Karur, Kongu Vellalar Groom in Karur, Kongu Groom"
        ></meta>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      {/* <Navbar transparent /> */}
      <main className="profile-page">
        <section className="relative block h-500-px">
          <div
            className="absolute top-0 w-full h-full bg-blueGray-800 bg-no-repeat bg-full"
            style={{
              backgroundImage:
                "url('https://demos.creative-tim.com/notus-nextjs/img/register_bg_2.png')",
            }}
          >
            <span
              id="blackOverlay"
              className="w-full h-full absolute opacity-0 bg-black"
            ></span>
          </div>
          <div
            className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-16"
            style={{ transform: "translateZ(0)" }}
          >
            <svg
              className="absolute bottom-0 overflow-hidden"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="none"
              version="1.1"
              viewBox="0 0 2560 100"
              x="0"
              y="0"
            >
              <polygon
                className="text-blueGray-200 fill-current"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </section>
        <section className="relative py-16 bg-white">
          <div className="container mx-auto px-4">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
              <div className="px-6">
                <div className="flex flex-wrap justify-center">
                  <div className="w-full lg:w-3/12 px-4 lg:order-2 flex justify-center">
                    <div className="relative profile-image">
                      <Image
                        alt="Maniraj Murugan Image"
                        height="500"
                        width="500"
                        priority={true}
                        src="/profile/manmur_profile.jpg"
                        className="shadow-xl rounded-md h-auto align-middle border-none absolute -m-16 -ml-20 lg:-ml-16 max-w-150-px"
                      />
                    </div>
                  </div>
                </div>
                <div className="text-center">
                  <h6 className="text-md font-bold mb-8">
                    All the information provided below are genuine and honest
                  </h6>
                  <h3 className="text-4xl font-semibold leading-normal mb-2 text-blueGray-700 mb-2">
                    Maniraj Murugan
                  </h3>
                  <div className="text-sm leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                    <i className="fas fa-map-marker-alt mr-2 text-lg text-blueGray-400"></i>{" "}
                    Velayuthampalayam, Karur
                  </div>
                  <div className="mb-2 text-blueGray-600 mt-10">
                    <i className="fas fa-briefcase mr-2 text-lg text-blueGray-400"></i>
                    Software Engineer - Front End Web Developer - Hidden Brains
                    Infotech
                  </div>
                  <div className="mb-2 text-blueGray-600">
                    <i className="fas fa-university mr-2 text-lg text-blueGray-400"></i>
                    Civil Engineering - Adhiyamaan College of Engineering
                  </div>
                </div>
                <div className="mt-10 py-10 border-t border-blueGray-200 text-center">
                  <div className="flex flex-wrap justify-center">
                    <div className="w-full lg:w-9/12 px-4">
                      <h6 className="text-xl font-semibold italic">
                        &quot;Civil Engineer by graduation and Software Engineer
                        by Profession&quot;
                      </h6>
                      <p className="mb-4 text-lg leading-relaxed text-blueGray-700 mt-4">
                        When life throws lemons at you make lemonade.. This
                        pretty much sums up my life so far.
                      </p>
                      <p className="mb-4 text-lg leading-relaxed text-blueGray-700 mt-4">
                        I am looking for a educated and like minded person with
                        a correct attitude, right understanding and immense
                        love.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="py-10 bg-blueGray-200 -mt-24">
          <div className="container mx-auto px-4">
            <div className="flex flex-wrap">
              <div className="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                  <div className="px-4 py-5 flex-auto">
                    <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-2 shadow-lg rounded-full bg-red-400">
                      <i className="fas fa-award"></i>
                    </div>
                    <h6 className="text-xl font-semibold mb-2">Work</h6>
                    <h4 className="text-md"> Senior Software Engineer </h4>
                    <a
                      className="text-indigo-600"
                      rel="noreferrer"
                      href="https://www.hiddenbrains.com/"
                      target="_blank"
                    >
                      {" "}
                      Hiddenbrains Infotech{" "}
                    </a>
                    <p className="mt-2 mb-4 text-blueGray-500">
                      It is a regular full time job and I am in{" "}
                      <b>Permanent Work from Home</b> role and I can give 100%
                      assurance that I will never travel to the company location
                      and I have got email in written for the same. So I can
                      consider the relocation if needed as per bride&apos;s
                      choice.
                    </p>
                    <p className="mt-2 mb-4 text-blueGray-500">
                      I am open to discuss my job details and salary details in
                      person and again I will be true and genuine in disclosing
                      all the details.
                    </p>
                  </div>
                </div>
              </div>

              <div className="w-full md:w-4/12 px-4 text-center">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                  <div className="px-4 py-5 flex-auto">
                    <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-lightBlue-400">
                      <i className="fas fa-retweet"></i>
                    </div>
                    <h6 className="text-xl font-semibold">Family Details</h6>
                    <p className="mt-2 mb-4 font-bold">
                      Typical Middle class Family
                    </p>
                    <ul className="mt-2 mb-4 text-blueGray-500 text-left p-4">
                      <li>
                        {" "}
                        Father - TNPL Contract Employee, Agriculture, Finance{" "}
                      </li>
                      <li> Mother - House Wife </li>
                      <li> Elder Sister - Married and settled in Chennai </li>
                    </ul>
                  </div>
                </div>
              </div>

              <div className="pt-6 w-full md:w-4/12 px-4 text-center">
                <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                  <div className="px-4 py-5 flex-auto">
                    <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-emerald-400">
                      <i className="fas fa-fingerprint"></i>
                    </div>
                    <h6 className="text-xl font-semibold">Assets</h6>
                    <p className="mt-2 mb-4 text-blueGray-500 text-left p-4">
                      We hold 2 floor own house, agriculture land, house plots
                      and partnership in finance and in detail asset details can
                      be given in person.
                    </p>
                    {/* <ul className="mt-2 mb-4 text-blueGray-500 text-left p-4">
                      <li> 2 Floor Own House </li>
                      <li>
                        {" "}
                        3 acres of land in which we are doing agriculture in 2
                        acres and 1 acre is empty land{" "}
                      </li>
                      <li> 3 House plots </li>
                      <li> 2 Finance Partner </li>
                    </ul> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="pb-10 white">
          <div className="relative bg-blueGray-800 md:pt-32 pb-10 pt-12">
            <div className="px-4 md:px-10 mx-auto w-full">
              <div className="flex flex-wrap">
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative flex flex-col min-w-0 break-words bg-blueGray-800 rounded-lg mb-6 xl:mb-0 shadow-lg">
                    <div className="flex-auto p-2">
                      <Image
                        alt="Maniraj Murugan"
                        width={680}
                        height={860}
                        priority={true}
                        src="/profile/manmur_shirt_new.jpg"
                        className="bg-white w-full mb-8 shadow-lg rounded-lg"
                      />
                    </div>
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative flex flex-col min-w-0 break-words bg-blueGray-800 rounded-lg mb-6 xl:mb-0 shadow-lg">
                    <div className="flex-auto p-2">
                      <Image
                        alt="Maniraj Murugan"
                        width={680}
                        height={400}
                        priority={true}
                        src="/profile/maniraj_new.jpg"
                        className="bg-white w-full mb-8 shadow-lg rounded-lg"
                      />
                    </div>
                  </div>
                </div>
                <div className="w-full lg:w-4/12 px-4">
                  <div className="relative flex flex-col min-w-0 break-words bg-blueGray-800 rounded-lg mb-6 xl:mb-0 shadow-lg">
                    <div className="flex-auto p-2">
                      <Image
                        alt="Maniraj Murugan"
                        width={680}
                        height={860}
                        priority={true}
                        src="/profile/maniraj_avatar.jpg"
                        className="bg-white w-full mb-8 shadow-lg rounded-lg"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>

      <div className="w-full text-center py-10 px-4">
        <h4 className="text-3xl font-semibold">
          You can also view my social profiles here
        </h4>
        <div className="mt-6 lg:mb-0 mb-6">
          <a
            className="bg-white text-lightBlue-400 shadow-lg font-normal h-10 w-10 p-4 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
            href="https://twitter.com/Maniraj_Murugan"
            rel="noreferrer"
            target="_blank"
          >
            <i className="fab fa-twitter"></i>
          </a>
          <a
            className="bg-white text-lightBlue-400 shadow-lg font-normal h-10 w-10 p-4 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
            href="https://www.linkedin.com/in/maniraj-murugan-84076b91"
            rel="noreferrer"
            target="_blank"
          >
            <i className="fab fa-linkedin-in"></i>
          </a>
          <a
            className="bg-white text-lightBlue-400 shadow-lg font-normal h-10 w-10 p-4 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
            href="https://stackoverflow.com/users/7785337/maniraj-murugan"
            rel="noreferrer"
            target="_blank"
          >
            <i className="fab fa-stack-overflow"></i>
          </a>
          <a
            className="bg-white text-lightBlue-400 shadow-lg font-normal h-10 w-10 p-4 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2"
            href="https://www.facebook.com/mani.raj.526"
            rel="noreferrer"
            target="_blank"
          >
            <i className="fab fa-facebook-square"></i>
          </a>
        </div>
      </div>
      <div className="text-center px-4">
        <p className="mb-4 text-lg leading-relaxed text-blueGray-700 mt-4">
          If you still want to know more about me, then you can go to
          google and search
          <span className="text-md font-semibold italic px-2">
            Maniraj Murugan
          </span>
        </p>
        <p className="text-lg">Thank You !</p>
      </div>
      <hr className="mt-6 border-blueGray-300" />
      <div className="flex flex-wrap items-center md:justify-between justify-center py-4">
        <div className="w-full md:w-4/12 px-4 mx-auto text-center">
          <div className="text-sm text-blueGray-500 font-semibold py-1">
            Copyright © {new Date().getFullYear()} Maniraj Murugan
          </div>
        </div>
      </div>
    </>
  );
}
